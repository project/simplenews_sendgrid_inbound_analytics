<?php

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function simplenews_sendgrid_inbound_analytics_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'simplenews_sendgrid_inbound_analytics') . '/includes/views',
    ),
    'handlers' => array(
      'simplenews_sendgrid_inbound_analytics_handler_open_count' => array(
        'parent' => 'views_handler_field',
      ),
      'simplenews_sendgrid_inbound_analytics_handler_click_count' => array(
        'parent' => 'views_handler_field',
      ),
      'simplenews_sendgrid_inbound_analytics_handler_detail_link' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function simplenews_sendgrid_inbound_analytics_views_data(){

    $data = array();

	$data['simplenews_sendgrid_inbound_analytics']['table']['group'] = t('Simplenews Sendgrid Inbound Analytics');
	$data['simplenews_sendgrid_inbound_analytics']['table']['join'] = array(
	    '#global' => array(),
	);

	// The Open Count field
    $data['simplenews_sendgrid_inbound_analytics']['open_count'] = array(
       'title' => t('Open Count'),
       'help' => t('Open Count'),
       'field' => array(
           'handler' => 'simplenews_sendgrid_inbound_analytics_handler_open_count',
       ),
       'sort' => array(
           'handler' => 'views_handler_sort',
       ),
       'filter' => array(
           'handler' => 'views_handler_filter_numeric',
       ),
    );

    // The Click Count field
    $data['simplenews_sendgrid_inbound_analytics']['click_count'] = array(
       'title' => t('Click Count'),
       'help' => t('Click Count'),
       'field' => array(
           'handler' => 'simplenews_sendgrid_inbound_analytics_handler_click_count',
       ),
       'sort' => array(
           'handler' => 'views_handler_sort',
       ),
       'filter' => array(
           'handler' => 'views_handler_filter_numeric',
       ),
    );

    // The Details Link field
    $data['simplenews_sendgrid_inbound_analytics']['detail_link'] = array(
       'title' => t('Details'),
       'help' => t('Details Link'),
       'field' => array(
           'handler' => 'simplenews_sendgrid_inbound_analytics_handler_detail_link',
       ),
    );

    // The Node ID field
	$data['sendgrid_inbound_analytics_table']['nid'] = array(
	    'title' => t('Nid'),
	    'help' => t('The Node ID'),
	    'field' => array(
	        'handler' => 'views_handler_field_numeric',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_numeric',
	    ),
	    'relationship' => array(
	        'base' => 'node',
	        'field' => 'nid',
	        'handler' => 'views_handler_relationship',
	        'label' => t('Node'),
	    ),
	    'argument' => array(
	        'handler' => 'views_handler_argument_node_nid',
	        'numeric' => TRUE,
	        'validate type' => 'nid',
	    ),
	);

    // The Taxonomy ID field
	$data['sendgrid_inbound_analytics_table']['tid'] = array(
	    'title' => t('tid'),
	    'help' => t('The Taxonomy ID'),
	    'field' => array(
	        'handler' => 'views_handler_field_numeric',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_numeric',
	    ),
	    'relationship' => array(
	        'base' => 'taxonomy_term_data',
	        'field' => 'tid',
	        'handler' => 'views_handler_relationship',
	        'label' => t('Taxonomy Term Data'),
	    ),
	    'argument' => array(
	        'handler' => 'views_handler_argument_term_node_tid',
	        'numeric' => TRUE,
	        'validate type' => 'tid',
	    ),
	);

	// The Sendgrid mailsub field
	$data['sendgrid_inbound_analytics_table']['mailsub'] = array(
	    'title' => t('mailsub'),
	    'help' => t('Sendgrid mailsub.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

	// The Sendgrid mailrecipient field
	$data['sendgrid_inbound_analytics_table']['mailrecipient'] = array(
	    'title' => t('mailrecipient'),
	    'help' => t('Sendgrid mailrecipient.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

	$data['sendgrid_inbound_analytics_table']['table']['join'] = array(
	    'taxonomy_term_data' => array(
	        'left_field' => 'tid',
	        'field' => 'tid',
	    ),
	);

	$data['sendgrid_inbound_analytics_table']['table']['join'] = array(
	    'users' => array(
	        'left_field' => 'nid',
	        'field' => 'nid',
	    ),
	);

    return $data;

}
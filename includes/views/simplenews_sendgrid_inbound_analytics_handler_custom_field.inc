<?php

/**
 * @file
 * Definition of simplenews_sendgrid_inbound_analytics_handler_open_count
 */
  
/**
 * Provides a custom views field.
 */
class simplenews_sendgrid_inbound_analytics_handler_open_count extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
  
  function query() {
    // do nothing -- to override the parent query.
  }
  
  function render($data) {
    $nid = $data->sendgrid_inbound_analytics_table_nid;
    $tid = $data->sendgrid_inbound_analytics_table_tid;
    $output = '';
    if(!empty($nid) && !empty($tid)){
      $output = db_query("SELECT * FROM {sendgrid_inbound_analytics_table} WHERE nid=$nid and tid=$tid and event='open'")->rowCount();
    }
     
    return $output; 
  }
}

class simplenews_sendgrid_inbound_analytics_handler_click_count extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
  
  function query() {
    // do nothing -- to override the parent query.
  }
  
  function render($data) {
    $nid = $data->sendgrid_inbound_analytics_table_nid;
    $tid = $data->sendgrid_inbound_analytics_table_tid;
    $output = '';
    if(!empty($nid) && !empty($tid)){
      $output = db_query("SELECT * FROM {sendgrid_inbound_analytics_table} WHERE nid=$nid and tid=$tid and event='click'")->rowCount();
    }

    return $output; 
  }
}


class simplenews_sendgrid_inbound_analytics_handler_detail_link extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
  
  function query() {
    // do nothing -- to override the parent query.
  }
  
  function render($data) {
    $nid = $data->sendgrid_inbound_analytics_table_nid;
    $tid = $data->sendgrid_inbound_analytics_table_tid;
    $output = '';
    if(!empty($nid) && !empty($tid)){
      $output = '<a href="/simplenews-sendgrid-details/'.$nid.'/'.$tid.'">Details</a>';
    }

    return $output; 
  }
}
<?php
/**
* Implementation of hook_views_default_views().
**/
function simplenews_sendgrid_inbound_analytics_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'simplenews_sendgrid_inbound_analytics';
  $view->description = 'Display a list of Simplenews Sendgrid Inbound Analytics.';
  $view->tag = 'simplenews_sendgrid_inbound_analytics';
  $view->base_table = 'sendgrid_inbound_analytics_table';
  $view->human_name = 'Simplenews Sendgrid Inbound Analytics';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Simplenews Sendgrid Inbound Analytics';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = 50;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'name' => 'name',
    'timestamp' => 'timestamp',
    'open_count' => 'open_count',
    'click_count' => 'click_count',
    'detail_link' => 'detail_link',
  );
  $handler->display->display_options['style_options']['default'] = 'name';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'timestamp' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'open_count' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'click_count' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'detail_link' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'nid';
  $handler->display->display_options['fields']['title']['label'] = 'Title';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'tid';
  $handler->display->display_options['fields']['name']['label'] = 'Category Name';
  /* Field: Simplenews Sendgrid Inbound Analytics: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['label'] = 'Date/Time';
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'medium';
  /* Field: Simplenews Sendgrid Inbound Analytics: Open Count */
  $handler->display->display_options['fields']['open_count']['id'] = 'open_count';
  $handler->display->display_options['fields']['open_count']['table'] = 'simplenews_sendgrid_inbound_analytics';
  $handler->display->display_options['fields']['open_count']['field'] = 'open_count';
  $handler->display->display_options['fields']['open_count']['label'] = 'Open Count';
  /* Field: Simplenews Sendgrid Inbound Analytics: Click Count */
  $handler->display->display_options['fields']['click_count']['id'] = 'click_count';
  $handler->display->display_options['fields']['click_count']['table'] = 'simplenews_sendgrid_inbound_analytics';
  $handler->display->display_options['fields']['click_count']['field'] = 'click_count';
  $handler->display->display_options['fields']['click_count']['label'] = 'Click Count';
  /* Field: Simplenews Sendgrid Inbound Analytics: Details Link */
  $handler->display->display_options['fields']['detail_link']['id'] = 'detail_link';
  $handler->display->display_options['fields']['detail_link']['table'] = 'simplenews_sendgrid_inbound_analytics';
  $handler->display->display_options['fields']['detail_link']['field'] = 'detail_link';
  $handler->display->display_options['fields']['detail_link']['label'] = 'Details';

  /* Relationship: Simplenews Sendgrid Inbound Analytics: Node ID*/
  $handler->display->display_options['relationships']['nid']['id'] = 'nid';
  $handler->display->display_options['relationships']['nid']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['relationships']['nid']['field'] = 'nid';

  /* Relationship: Simplenews Sendgrid Inbound Analytics: Subscriber Email*/
  $handler->display->display_options['relationships']['email']['id'] = 'email';
  $handler->display->display_options['relationships']['email']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['relationships']['email']['field'] = 'email';

  /* Relationship: Simplenews Sendgrid Inbound Analytics: Taxonomy ID*/
  $handler->display->display_options['relationships']['tid']['id'] = 'tid';
  $handler->display->display_options['relationships']['tid']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['relationships']['tid']['field'] = 'tid';

  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'nid';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'simplenews' => 'simplenews',
  );

  /* Sort criterion: Sendgrid Inbound Analytics: timestamp */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';

  /* Display: 	Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'sendgrid-inbound-analytics-table';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'Simplenews Sendgrid Inbound Analytics';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Simplenews Sendgrid Inbound Analytics';
  $handler->display->display_options['tab_options']['description'] = 'Manage Simplenews Sendgrid Inbound Analytics.';
  $handler->display->display_options['tab_options']['weight'] = '';
  $handler->display->display_options['tab_options']['name'] = 'management';

  $translatables['sendgrid_inbound_analytics_table'] = array(
    t('Defaults'),
    t('Simplenews Sendgrid Inbound Analytics'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Nid'),
    t('tid'),
    t('timestamp'),
    t('Open Count'),
    t('Click Count'),
    t('Details'),
    t('Page'),
    t('There are currently no list.'),
  );

  $views[$view->name] = $view;

  // Simplenews Sendgrid Inbound Analytics Details Page View

  $view = new view();
  $view->name = 'simplenews_sendgrid_inbound_analytics_details';
  $view->description = 'Display a list of Simplenews Sendgrid Inbound Analytics Details Per Node.';
  $view->tag = 'Simplenews Sendgrid Inbound Analytics Details';
  $view->base_table = 'sendgrid_inbound_analytics_table';
  $view->human_name = 'Simplenews Sendgrid Inbound Analytics Details';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Simplenews Sendgrid Inbound Analytics Details';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = 50;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'name' => 'name',
    'timestamp' => 'timestamp',
    'email' => 'email',
    'event' => 'event',
  );
  $handler->display->display_options['style_options']['default'] = 'timestamp';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'timestamp' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'email' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'event' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'nid';
  $handler->display->display_options['fields']['title']['label'] = 'Title';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'tid';
  $handler->display->display_options['fields']['name']['label'] = 'Category Name';
  /* Field: Simplenews Sendgrid Inbound Analytics: Timestamp */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['label'] = 'Timestamp';
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'medium';
  /* Field: Simplenews Sendgrid Inbound Analytics: Email */
  $handler->display->display_options['fields']['email']['id'] = 'email';
  $handler->display->display_options['fields']['email']['table'] = 'simplenews_sendgrid_inbound_analytics';
  $handler->display->display_options['fields']['email']['field'] = 'email';
  $handler->display->display_options['fields']['email']['label'] = 'Email';
  /* Field: Simplenews Sendgrid Inbound Analytics: Event */
  $handler->display->display_options['fields']['event']['id'] = 'event';
  $handler->display->display_options['fields']['event']['table'] = 'simplenews_sendgrid_inbound_analytics';
  $handler->display->display_options['fields']['event']['field'] = 'event';
  $handler->display->display_options['fields']['event']['label'] = 'Event';

  /* Relationship: Simplenews Sendgrid Inbound Analytics: Node ID*/
  $handler->display->display_options['relationships']['nid']['id'] = 'nid';
  $handler->display->display_options['relationships']['nid']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['relationships']['nid']['field'] = 'nid';

  /* Relationship: Simplenews Sendgrid Inbound Analytics: Subscriber Email*/
  $handler->display->display_options['relationships']['email']['id'] = 'email';
  $handler->display->display_options['relationships']['email']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['relationships']['email']['field'] = 'email';

  /* Relationship: Simplenews Sendgrid Inbound Analytics: Taxonomy ID*/
  $handler->display->display_options['relationships']['tid']['id'] = 'tid';
  $handler->display->display_options['relationships']['tid']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['relationships']['tid']['field'] = 'tid';

  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'nid';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'simplenews' => 'simplenews',
  );

  /* Sort criterion: Sendgrid Inbound Analytics: timestamp */
  $handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';

  /* Contextual filter: Sendgrid Inbound Analytics: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['nid']['default_argument_options']['index'] = '1';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '50';
  /* Contextual filter: Sendgrid Inbound Analytics: tid */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'sendgrid_inbound_analytics_table';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['tid']['default_argument_options']['index'] = '2';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '50';

  /* Display: 	Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'simplenews-sendgrid-details/%/%';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'Simplenews Sendgrid Inbound Analytics Details';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Simplenews Sendgrid Inbound Analytics Details';
  $handler->display->display_options['tab_options']['description'] = 'Manage Simplenews Sendgrid Inbound Analytics Details.';
  $handler->display->display_options['tab_options']['weight'] = '';
  $handler->display->display_options['tab_options']['name'] = 'management';

  $translatables['sendgrid_inbound_analytics_table'] = array(
    t('Defaults'),
    t('Simplenews Sendgrid Inbound Analytics'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Nid'),
    t('tid'),
    t('timestamp'),
    t('email'),
    t('event'),
    t('Page'),
    t('There are currently no list.'),
  );

  $views[$view->name] = $view;

  return $views;
}